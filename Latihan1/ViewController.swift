//
//  ViewController.swift
//  Latihan1
//
//  Created by Macbook Pro on 17/03/23.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var astronoutButton: UIButton!
    @IBOutlet weak var policeButton: UIButton!
    @IBOutlet weak var cheffButton: UIButton!
    @IBOutlet weak var enginerButton: UIButton!
    
    //MARK : - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    // MARK : - Actions
    
    
    @IBAction func policeButtonClicked(_ sender: Any) {
        showAlert(with: "Pekerjaan Kamu", and: "Polisi")
    }
    
    @IBAction func cheffButtonClicked(_ sender: Any) {
        showAlert(with: "Pekerjaan Kamu", and: "Koki")
    }
    @IBAction func astronoutButtonClicked(_ sender: Any) {
        showAlert(with: "Pekerjaan Kamu", and: "Astronot")
    }
    
    @IBAction func engineerButtonClicked(_ sender: Any) {
        showAlert(with: "Pekerjaan Kamu", and: "Tukang")
    }
    
    
    // MARK : - Alert
    
    func showAlert(with title:String,and subtitle :String){
        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        present(alert, animated: true)
    }
    
}

